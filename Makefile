out=build
pdf_out=$(out)/pdf
html_out=$(out)/html
merge_name=$(pdf_out)/merge.pdf

src=$(wildcard **/*.rst)
src += $(wildcard *.rst)
pdf=$(patsubst %.rst, $(pdf_out)/%.pdf, $(src))
html=$(patsubst %.rst, $(html_out)/%.html, $(src))

$(pdf_out)/%.pdf: %.rst
	mkdir -p $(dir $@)
	pandoc $^ -o $@

$(html_out)/%.html: %.rst
	mkdir -p $(dir $@)
	pandoc $^ -o $@

$(merge_name): $(pdf)
	pdfunite $^ $@

pdf: $(pdf)

html: $(html)

all: html

clean: 
	rm -r $(out)

.PHONY: html all clean pdf
.DEFAULT_GOAL := html
