Making xbacklight work on arch
==============================

Make sure current user is part of video group.

Add the following to the file ``/etc/udev/rules.d/backlight.rules``

::

   ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chgrp video /sys/class/backlight/%k/brightness"
   ACTION=="add", SUBSYSTEM=="backlight", KERNEL=="acpi_video0", RUN+="/bin/chmod g+w /sys/class/backlight/%k/brightness"

Install the acpilight instead of standard xorg-xbacklight.
